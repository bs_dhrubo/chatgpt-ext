let observer;
const divSelector = "div.markdown.prose.w-full.break-words";
const pSelector = "p";

// function startTypingSound() {
// 	var audio = new Audio("typing-sound.mp3");
// 	audio.loop = true;
// 	audio.play();
// }

// function stopTypingSound() {
// 	var audio = new Audio("typing-sound.mp3");
// 	audio.pause();
// 	audio.currentTime = 0;
// }

function observeChatGPT(param) {
	window.alert(param);
	observer = new MutationObserver((mutations) => {
		window.alert("mutations"+mutations.length);
		mutations.forEach((mutation) => {
			const addedNodes = Array.from(mutation.addedNodes);
			const hasNewDivElement = addedNodes.some((node) =>
				node.matches(divSelector)
			);
			const hasNewParagraphElement = addedNodes.some(
				(node) => node.tagName === pSelector.toUpperCase()
			);
			window.alert(hasNewDivElement, hasNewParagraphElement);
			if (hasNewDivElement && hasNewParagraphElement) {
				window.alert("got it new p");
			}
		});
	});
	observer.observe(document.body, {
		attributes: true,
		childList: true,
		subtree: true,
	});
}

chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
	if (
		changeInfo.status == "complete" &&
		tab.url.indexOf("chat.openai.com/chat") > -1
	) {
		window.alert("if " + changeInfo.status + tab.url);
		observeChatGPT("calling");
	}
});
